// super_hexagon_solver project main.go
package main

import (
	"code.google.com/p/graphics-go/graphics"
	"ellipse"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"interpClosest"
	"log"
	"math"
	"os"
	"raster"
	"runtime"
	"strconv"
	"time"
	"x11"
)

// https://code.google.com/p/vp8-go/source/browse/#hg%2Fvp8

// Save images
func debug() bool {
	//return true
	return false
}

// Print a preposterous amount of info to stdout
func vomit() bool {
	//return true
	return false
}

func useSample() bool {
	//return true
	return false
}

func getWidth() int {
	return 250
}
func getHeight() int {
	return 250
}

func SampleScreenshot() (img draw.Image, err error) {
	//inFilename := "/home/awm10/Pictures/super hexagon screencapture/Screenshot from 2014-05-28 16:10:39.png"
	//inFilename := "/home/awm10/Pictures/super hexagon screencapture/Screenshot from 2014-05-29 23:40:15.png"
	//inFilename := "/home/awm10/Pictures/super hexagon screencapture/Screenshot from 2014-05-29 23:38:28.png"
	//inFilename := "/home/awm10/Pictures/super hexagon screencapture/Screenshot from 2014-05-29 23:46:31.png"
	inFilename := "/home/awm10/Pictures/super hexagon screencapture/semicircle-flipped.png"
	r, err := os.Open(inFilename) // For read access.
	if err != nil {
		log.Fatal(err)
	}
	src, err := png.Decode(r)
	if err != nil {
		return nil, err
	}
	img = src.(draw.Image)
	return img, nil
}
func getCroppingBounds(img image.Image) (minX, maxX int) {
	yIncr := img.Bounds().Size().Y / 100
	leftX := img.Bounds().Max.X
	rightX := img.Bounds().Min.X
	for y := img.Bounds().Min.Y; y < img.Bounds().Max.Y; y += yIncr {
		for x := img.Bounds().Min.X; x < leftX; x++ {
			brightness := raster.Brightness(img.At(x, y))
			if brightness > 0 {
				leftX = x
			}
		}
		for x := img.Bounds().Max.X; x > rightX; x-- {
			brightness := raster.Brightness(img.At(x, y))
			if brightness > 0 {
				rightX = x
			}
		}
	}
	return leftX, rightX
}

func getBrightnessBounds(img image.Image) (min, max uint8) {
	y := img.Bounds().Min.Y + img.Bounds().Size().Y/2
	min = 0xFF
	max = 0x00
	leftX := img.Bounds().Min.X + img.Bounds().Size().X/4
	rightX := img.Bounds().Max.X - img.Bounds().Size().X/4
	for x := leftX; x < rightX; x++ {
		brightness := raster.Brightness(img.At(x, y))
		if brightness < min {
			min = brightness
		}
		if brightness > max {
			max = brightness
		}
	}
	return min, max
}

func connectBlocking(img draw.Image, y int) {
	bounds := img.Bounds()
	for leftX := bounds.Min.X; leftX < bounds.Max.X; leftX++ {
		if raster.Brightness(img.At(leftX, y)) == 0xFF {
			hasExitedWhite := false
			rightX := leftX
			for ; rightX < bounds.Max.X && rightX < leftX+getWidth()/4; rightX++ {
				if hasExitedWhite {
					if raster.Brightness(img.At(rightX, y)) == 0xFF {
						for x := leftX; x < rightX; x++ {
							img.Set(x, y, color.Gray{0xFF})
						}
						break
					}
				} else {
					if raster.Brightness(img.At(rightX, y)) != 0xFF {
						hasExitedWhite = true
						leftX = rightX
					}
				}
			}
			leftX = rightX - 1
		}
	}
}

func cropFilterScale(img draw.Image, src image.Image) {
	srcLeftBound, srcRightBound := getCroppingBounds(src)
	srcXSize := srcRightBound - srcLeftBound
	minValue, maxValue := getBrightnessBounds(src)
	valueRange := uint16(maxValue - minValue)
	if valueRange < 1 {
		valueRange = 1
	}
	for y := img.Bounds().Min.Y; y < img.Bounds().Max.Y; y++ {
		ySrc := (y-img.Bounds().Min.Y)*src.Bounds().Size().Y/img.Bounds().Size().Y + src.Bounds().Min.Y
		for x := img.Bounds().Min.X; x < img.Bounds().Max.X; x++ {
			xSrc := (x-img.Bounds().Min.X)*srcXSize/img.Bounds().Size().X + srcLeftBound
			val := raster.Brightness(src.At(xSrc, ySrc))
			if val < minValue {
				val = 0x00
			} else if val > maxValue {
				val = 0xFF
			} else {
				val = uint8(uint16(val-minValue) * 0xFF / valueRange)
				if val >= 0x70 {
					val = 0xFF
				} else {
					val = 0x00
				}
			}
			img.Set(x, y, color.Gray{val})
		}
	}
	connectBlocking(img, img.Bounds().Min.Y)
	connectBlocking(img, img.Bounds().Min.Y+1)
	connectBlocking(img, img.Bounds().Max.Y-1)
	connectBlocking(img, img.Bounds().Max.Y-2)
}

func saveOutput(img image.Image, label int, ct int, time int64) {
	name := strconv.FormatInt(time, 10) + "-" + strconv.Itoa(ct) + "-" + strconv.Itoa(label)
	outFilename := "/home/awm10/Desktop/bulk/" + name + ".png"
	w, err := os.Create(outFilename) // For write access
	if err != nil {
		return
	}
	png.Encode(w, img)
	defer func() {
		if err := w.Close(); err != nil {
			panic(err)
		}
	}()
}

func FindShip(img draw.Image) image.Point {
	img2 := image.NewGray(img.Bounds())
	draw.Draw(img2, img2.Bounds(), img, img.Bounds().Min, draw.Src)
	if vomit() {
		fmt.Println(time.Now(), "FindShipBySize()")
	}
	location, ok := FindShipBySize(img)
	if ok {
		return location
	}
	if vomit() {
		fmt.Println(time.Now(), "FindShipByWidth()")
	}
	location = FindShipByWidth(img2)
	return location
}

func FindShipBySize(img draw.Image) (location image.Point, ok bool) {
	bestCenter := image.Pt(0, 0)
	minSmallestSize := 7 * (getWidth() * getHeight()) / (350 * 350)
	maxSmallestSize := 30 * (getWidth() * getHeight()) / (350 * 350)
	smallestSize := img.Bounds().Size().X * img.Bounds().Size().Y
	center := raster.Center(img)
	minR := raster.Distance(img.Bounds().Min, img.Bounds().Max) / 25 * float64(getWidth()) / 350
	minR2 := math.Pow(minR, 2)
	maxR := raster.Distance(img.Bounds().Min, img.Bounds().Max) / 7 * float64(getWidth()) / 350
	maxR2 := math.Pow(maxR, 2)
	// I seem to have reversed the yOffset<->y naming convention between here and in ellipse.
	for yOffset := maxR; yOffset >= 0; yOffset-- {
		yOffset2 := math.Pow(yOffset, 2)
		for xOffset := math.Sqrt(maxR2 - yOffset2); math.Pow(xOffset, 2) >= minR2-yOffset2 && xOffset > 0; xOffset-- {
			for i := 0; i < 4; i++ {
				ySign := 1
				xSign := 1
				if i%2 == 1 {
					ySign = -1
				}
				if (i>>1)%2 == 1 {
					xSign = -1
				}
				x := int(xOffset)*xSign + center.X
				y := int(yOffset)*ySign + center.Y
				if raster.Brightness(img.At(x, y)) == 0xFF {
					blobCenter, size := raster.FloodFill8(img, x, y, color.Gray{0xA0})
					if raster.Distance(center, blobCenter) <= maxR &&
						size >= minSmallestSize &&
						size <= maxSmallestSize {
						bestCenter = blobCenter
						smallestSize = size
					}
				}
			}
		}
	}
	ok = (smallestSize <= maxSmallestSize) &&
		(smallestSize >= minSmallestSize) &&
		raster.Distance(center, bestCenter) <= maxR
	raster.FloodFill8(img, bestCenter.X, bestCenter.Y, color.Gray{0x80})
	return bestCenter, ok
}

func FindShipByWidth(img draw.Image) image.Point {
	center := raster.RectangleCenter(img.Bounds())
	centerColor := img.At(center.X, center.Y)
	spokes := 360
	boardDiagonal := raster.Distance(
		image.Pt(img.Bounds().Min.X, img.Bounds().Min.Y),
		image.Pt(img.Bounds().Max.X, img.Bounds().Max.Y))
	maxHexagonInnerWidth := boardDiagonal / 4
	maxHexagonOuterRadius := boardDiagonal / 3.9
	maxShipInnerRadius := boardDiagonal / 3.8
	maxShipOuterRadius := boardDiagonal / 3
	shipStartRadii := make([]float64, spokes)
	for i := 0; i < spokes; i++ {
		th := float64(i) / float64(spokes) * 2 * math.Pi
		r := float64(0)
		// Hit the center inside of the hexagon border
		for ; r < maxHexagonInnerWidth; r++ {
			pt := image.Pt(
				center.X+int(r*math.Cos(th)),
				center.Y+int(r*math.Sin(th)))
			currColor := img.At(pt.X, pt.Y)
			if currColor != centerColor {
				break
			}
		}
		if r >= maxHexagonInnerWidth {
			continue
		}
		// Leave the center hexagon border
		for ; r < maxHexagonOuterRadius; r++ {
			pt := image.Pt(
				center.X+int(r*math.Cos(th)),
				center.Y+int(r*math.Sin(th)))
			currColor := img.At(pt.X, pt.Y)
			if currColor == centerColor {
				break
			}
		}
		if r >= maxHexagonOuterRadius {
			continue
		}
		r *= 1.05
		// Go to the start of the ship
		for ; r < maxShipInnerRadius; r++ {
			pt := image.Pt(
				center.X+int(r*math.Cos(th)),
				center.Y+int(r*math.Sin(th)))
			currColor := img.At(pt.X, pt.Y)
			if currColor != centerColor {
				break
			}
		}
		if r >= maxShipInnerRadius {
			continue
		}
		shipStartRadius := r
		// Find the thickness of the ship
		for ; r < maxShipOuterRadius; r++ {
			pt := image.Pt(
				center.X+int(r*math.Cos(th)),
				center.Y+int(r*math.Sin(th)))
			currColor := img.At(pt.X, pt.Y)
			if currColor == centerColor {
				break
			}
		}
		if r >= maxShipOuterRadius {
			continue
		}
		shipStartRadii[i] = shipStartRadius
	}
	// Want shortest contiguous radiusDifferences of nonzero length
	bestRadiusDifference := math.MaxFloat64
	bestSpoke := 0
	shipWidthMin := int(float64(spokes) * 0.01)
	shipWidthMax := int(float64(spokes) * 0.03)
	shipRadiusDifferenceCutoff := boardDiagonal/float64(spokes) + float64(1)
	for i := 0; i < spokes; i++ {
		radiusDifferenceAverage := shipStartRadii[i]
		shipWidth := 0
		for ; shipWidth < shipWidthMax; shipWidth++ {
			currShipStartRadius := shipStartRadii[(i+shipWidth)%spokes]
			nextShipStartRadius := shipStartRadii[(i+shipWidth+1)%spokes]
			if currShipStartRadius == 0 {
				break
			} else if math.Abs(currShipStartRadius-nextShipStartRadius) > shipRadiusDifferenceCutoff {
				break
			} else {
				shipWidth++
			}
			radiusDifferenceAverage += currShipStartRadius
		}
		radiusDifferenceAverage /= float64(shipWidth)
		if shipWidth < shipWidthMin || shipWidth > shipWidthMax {
			i += shipWidth
			continue
		}
		if radiusDifferenceAverage <= bestRadiusDifference {
			bestRadiusDifference = radiusDifferenceAverage
			bestSpoke = (i + shipWidth/2) % spokes
		}
	}
	bestShipRadius := shipStartRadii[bestSpoke]
	bestShipTheta := float64(bestSpoke) / float64(spokes) * (2 * math.Pi)
	point := image.Pt(
		int(center.X)+int(bestShipRadius*math.Cos(bestShipTheta)),
		int(center.Y)+int(bestShipRadius*math.Sin(bestShipTheta)))
	return point
}

type ShipState struct {
	Radius, Theta float64
	Direction     int
}

type ShipProperties struct {
	RadialVelocity, MaxAngularVelocity, ShipWidth float64
}

type DecisionNode struct {
	Direction int
	EndRadius float64
	Score     float64
}

func getShipDirection_recurse(obstacles draw.Image, properties ShipProperties,
	state ShipState, cachedResults map[ShipState]DecisionNode) DecisionNode {
	decision, ok := cachedResults[state]
	if ok {
		return decision
	}
	boardCenter := raster.Center(obstacles)
	boardRadius := raster.Distance(boardCenter, obstacles.Bounds().Min)
	decision = DecisionNode{0, 0, 0}
	for direction := -1; direction <= 1; direction++ {
		nextState := state
		nextState.Direction = direction
		angularVelocity := 0.75*float64(direction)*properties.MaxAngularVelocity +
			0.25*float64(state.Direction)
		isBlocked := false
		isEscaped := false
		for incrementalRadius := float64(0); incrementalRadius < properties.RadialVelocity; incrementalRadius += 0.5 {
			nextState.Radius = state.Radius + incrementalRadius
			nextState.Theta = state.Theta + (incrementalRadius/properties.RadialVelocity)*angularVelocity
			thetaBlurWidth := properties.ShipWidth
			thetaBlurLeft := nextState.Theta - thetaBlurWidth
			thetaBlurRight := nextState.Theta + thetaBlurWidth
			for thetaBlur := thetaBlurLeft; thetaBlur <= thetaBlurRight; thetaBlur += thetaBlurWidth {
				x := boardCenter.X + int(nextState.Radius*math.Cos(thetaBlur))
				y := boardCenter.Y + int(nextState.Radius*math.Sin(thetaBlur))
				if !raster.RectangleContains(obstacles.Bounds(), image.Pt(x, y)) {
					nextState.Radius = boardRadius
					isEscaped = true
					break
				} else if raster.Brightness(obstacles.At(x, y)) == 0xFF {
					isBlocked = true
					break
				} else {
					if debug() {
						obstacles.Set(x, y, color.Gray{0x33})
					}
				}
			}
			if isBlocked || isEscaped {
				break
			}
		}
		currDecision := DecisionNode{direction, nextState.Radius, 0}
		if !isBlocked && !isEscaped {
			currDecision = getShipDirection_recurse(obstacles, properties, nextState, cachedResults)
			currDecision.Direction = direction
		}
		currDecision.Score = 0
		if direction == 0 {
			currDecision.Score = 1
		}
		if currDecision.EndRadius > decision.EndRadius ||
			(currDecision.EndRadius == decision.EndRadius && currDecision.Score > decision.Score) {
			decision = currDecision
		}
	}
	cachedResults[state] = decision
	//fmt.Println("decision", decision, state.Radius)
	return decision
}

func GetShipDirection(obstacles draw.Image, state ShipState) (direction int) {
	boardCenter := raster.Center(obstacles)
	boardRadius := raster.Distance(boardCenter, obstacles.Bounds().Min)
	properties := ShipProperties{boardRadius / 7, 2 * math.Pi / 11, 0.05}
	cachedStates := make(map[ShipState]DecisionNode)
	grossDecision := getShipDirection_recurse(obstacles, properties, state, cachedStates)
	decision := grossDecision
	// Try to use the minimum possible angular/radial velocity ratio
	for i := 0; i < 20 && decision.EndRadius < boardRadius; i++ {
		properties.RadialVelocity *= 0.96
		properties.MaxAngularVelocity *= 1.06
		properties.ShipWidth *= 0.9
		decision = getShipDirection_recurse(obstacles, properties, state, make(map[ShipState]DecisionNode))
	}
	if decision.EndRadius < boardRadius {
		//if vomit() {
		fmt.Println("going back to original radius")
		//}
		decision = grossDecision
	}
	return decision.Direction
}

func doStuff(thread int, in chan x11.Image, out chan bool) {
	img := image.NewGray(image.Rect(0, 0, getWidth(), getHeight()))
	img2 := image.NewGray(image.Rect(0, 0, getWidth(), getHeight()))
	ct := 0
	prevDirection := 0
	prevPrevDirection := 0
	for {
		processStartTime := time.Now()
		ct++
		screenshot := <-in
		if vomit() {
			fmt.Println()
			fmt.Println("==", strconv.Itoa(ct), "==")
			fmt.Println(time.Now(), "Screenshot()")
		}
		//if useSample() {
		//screenshot, _ := SampleScreenshot()
		//} else {
		//}
		//saveOutput(screenshot, ct, time.Now().UnixNano())
		// graphics.Scale() doesn't work, interpolation is too slow
		if vomit() {
			fmt.Println(time.Now(), "Scale-screenshot")
		}
		cropFilterScale(img, screenshot)
		screenshot.Free()
		//saveOutput(img, ct+3)
		if vomit() {
			fmt.Println(time.Now(), "ellipse.Corners()")
		}
		center := raster.Center(img)
		corners := ellipse.Corners(img, center)
		if vomit() {
			fmt.Println(time.Now(), "smallestEllipseContainingPoints()")
		}
		if len(corners) < 3 {
			out <- false
			continue
		}
		ell := ellipse.SmallestEllipseContainingPoints(corners)
		parametric := ell.Parametric()
		if vomit() {
			fmt.Println(time.Now(), "translating")
		}
		graphics.I.Translate(float64(img.Bounds().Min.X+img.Bounds().Max.X)/2-parametric.Center.X,
			float64(img.Bounds().Min.Y+img.Bounds().Max.Y)/2-parametric.Center.Y)
		if vomit() {
			fmt.Println(time.Now(), "rotate/scale")
		}
		rotateTransform := graphics.I.Rotate(-parametric.Rotation)
		xScale := float64(1.0)
		yScale := float64(1.0)
		if parametric.X_scale > parametric.Y_scale {
			xScale = parametric.Y_scale / parametric.X_scale
		} else {
			yScale = parametric.X_scale / parametric.Y_scale
		}
		scaleTransform := graphics.I.Scale(xScale, yScale)
		transform := rotateTransform.Mul(scaleTransform)
		//saveOutput(img, ct+1)
		img2 = image.NewGray(img.Bounds())
		transform.TransformCenter(img2, img, interpClosest.Closest)
		img, img2 = img2, img
		// Fill in the center of the hexagon
		//saveOutput(img, ct+4)
		if vomit() {
			fmt.Println(time.Now(), "FindShip()")
		}
		draw.Draw(img2, img2.Bounds(), img, img.Bounds().Min, draw.Src)
		shipPt := FindShip(img2)
		shipCenter := shipPt
		// Arbitrarily compensate for image processing delay
		if vomit() {
			fmt.Println(time.Now(), "Process path")
		}
		boardCenter := raster.Center(img)
		shipOffsetX := float64(shipCenter.X - boardCenter.X)
		shipOffsetY := float64(shipCenter.Y - boardCenter.Y)
		boardDiagonal := raster.Distance(img.Bounds().Min, img.Bounds().Max)
		shipRadius := boardDiagonal * 0.06
		shipTheta := math.Atan2(float64(shipOffsetY), float64(shipOffsetX))
		shipTheta += 2 * 1.5 * math.Pi * time.Since(processStartTime).Seconds() *
			(0.5*float64(prevDirection) + 0.5*float64(prevPrevDirection))
		raster.FloodFill8(img, shipCenter.X, shipCenter.Y, color.Gray{0x77})
		//fmt.Println("processingTime", float64(processingTime)/math.Pow(10, 9))
		shipCenter = image.Pt(
			boardCenter.X+int(shipRadius*math.Cos(shipTheta)),
			boardCenter.Y+int(shipRadius*math.Sin(shipTheta)))
		// Approximate with 1 ship radius outward/second, 1 revolution/second
		if vomit() {
			fmt.Println(time.Now(), "steering")
		}
		//shipState := ShipState{shipRadius, shipTheta, prevDirection}
		shipState := ShipState{shipRadius, shipTheta, 0}
		shipDirection := GetShipDirection(img, shipState)
		x11.PressKey(shipDirection, true)
		prevPrevDirection = prevDirection
		prevDirection = shipDirection
		if vomit() {
			if shipDirection > 0 {
				fmt.Println(time.Now(), "right   \t")
			} else if shipDirection < 0 {
				fmt.Println(time.Now(), "left    \t")
			} else {
				fmt.Println(time.Now(), "straight\t")
			}
		}
		//// Rotate the output to put the ship on top
		if debug() {
			if vomit() {
				fmt.Println(time.Now(), "output")
			}
			img2 = image.NewGray(img.Bounds())
			unRotateTransform := graphics.I.Rotate(parametric.Rotation)
			unRotateTransform.TransformCenter(img2, img, interpClosest.Closest)
			img, img2 = img2, img
			saveOutput(img, thread, ct+7, time.Now().UnixNano())
		}
		//sleepTime := 80000000 - time.Since(loopStartTime)
		//if sleepTime > 0 {
		//	time.Sleep(sleepTime)
		//}
		out <- true
		runtime.GC()
	}
}

func main() {
	for i := 4; i >= 1; i-- {
		fmt.Println(i, "...")
		time.Sleep(1 * time.Second)
	}
	fmt.Println("GO!")
	if debug() {
		os.RemoveAll("/home/awm10/Desktop/bulk")
		os.Mkdir("/home/awm10/Desktop/bulk", 0777)
	}
	concurrentSimulations := 1
	if !useSample() {
		x11.OpenDisplay()
		defer x11.CloseDisplay()
	}
	in := make(chan x11.Image, concurrentSimulations)
	out := make(chan bool, concurrentSimulations)
	total := 0
	for i := 0; i < concurrentSimulations; i++ {
		go doStuff(i, in, out)
	}
	for i := 0; i < 100000; i++ {
		if total >= concurrentSimulations {
			<-out
			total--
		}
		screenshot := x11.Screenshot()
		in <- screenshot
		total++
		//time.Sleep(40 * time.Millisecond)
		// TODO: This is done wrong.
		// The correct way would be to loop inside doStuff() and wait on the channel for refreshed data in
		// screenshot, img, and img2.
	}
}
