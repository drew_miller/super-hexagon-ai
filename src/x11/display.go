package x11

/*
#cgo CFLAGS: -std=gnu99
#cgo LDFLAGS: -lX11 -L/usr/include/X11
#include "x11stuff.h"
*/
import "C"

func OpenDisplay() {
	C.start()
}

func CloseDisplay() {
	C.end()
}

func Screenshot() Image {
	return Image{C.screenshot()}
}
