#pragma once

#include <stdbool.h>
#include <X11/Xlib.h>

void start();
void end();
void pressKey(int direction, bool pressDown);
XImage *screenshot();

Display *display;
Window root;
