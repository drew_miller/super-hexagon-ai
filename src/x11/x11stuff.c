#include "x11stuff.h"

// Send a fake keystroke event to an X window.
// by Adam Pierce - http://www.doctort.org/adam/
// This is public domain software. It is free to use by anyone for any purpose.

#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

// Function to create a keyboard event
XKeyEvent createKeyEvent(
		Display *display,
		Window *win,
        Window *winRoot,
		bool press,
        int keycode,
		int modifiers) {
    XKeyEvent event;

    event.display     = display;
    event.window      = *win;
    event.root        = *winRoot;
    event.subwindow   = None;
    event.time        = CurrentTime;
    event.x           = 1;
    event.y           = 1;
    event.x_root      = 1;
    event.y_root      = 1;
    event.same_screen = True;
    event.keycode     = XKeysymToKeycode(display, keycode);
    event.state       = modifiers;

    if(press) {
        event.type = KeyPress;
	} else {
        event.type = KeyRelease;
	}

    return event;
}

void pressKey(int direction, bool pressDown) {
	// The key code to be sent.
	// A full list of available codes can be found in /usr/include/X11/keysymdef.h
	int keyCode;
	if(direction > 0) {
		keyCode = XK_Right;
	} else if(direction < 0) {
		keyCode = XK_Left;
	} else {
		keyCode = 0;
	}

    if (keyCode == XK_Right && pressDown) {
		// Send a fake key press event to the window.
	    XKeyEvent event = createKeyEvent(display, &root, &root, true, XK_Right, 0);
	    XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    } else {
		// Send a fake key release event to the window.
		XKeyEvent event = createKeyEvent(display, &root, &root, false, XK_Right, 0);
	    XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    }
    if (keyCode == XK_Left && pressDown) {
		// Send a fake key press event to the window.
	    XKeyEvent event = createKeyEvent(display, &root, &root, true, XK_Left, 0);
	    XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    } else {
		// Send a fake key release event to the window.
		XKeyEvent event = createKeyEvent(display, &root, &root, false, XK_Left, 0);
	    XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    }
}

XImage *screenshot() {
	XWindowAttributes gwa;
	XGetWindowAttributes(display, root, &gwa);
	XImage *image = XGetImage(display, root, 0, 0, gwa.width, gwa.height, AllPlanes, ZPixmap);
	return image;
}

void start() {
	display = XOpenDisplay(NULL);
    int revert;
    XGetInputFocus(display, &root, &revert);
}

void end() {
	XCloseDisplay(display);
}
