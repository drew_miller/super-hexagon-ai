package x11

/*
#cgo CFLAGS: -std=gnu99
#cgo LDFLAGS: -lX11 -L/usr/include/X11
#include "x11stuff.h"
*/
import "C"

func PressKey(val int, down bool) {
	C.pressKey(C.int(val), C.bool(down))
}
