package x11

/*
#cgo CFLAGS: -std=gnu99
#cgo LDFLAGS: -lX11 -L/usr/include/X11
#include "x11stuff.h"
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>

unsigned long XGetPixel_go(XImage *ximage, int x, int y) {
	return XGetPixel(ximage, x, y);
}
*/
import "C"

import (
	"image"
	"image/color"
	"unsafe"
)

type Image struct {
	ximg *C.XImage
}

// ColorModel returns the Image's color model.
func (Image) ColorModel() color.Model {
	return color.RGBAModel
}

// Bounds returns the domain for which At can return non-zero color.
// The bounds do not necessarily contain the point (0, 0).
func (img Image) Bounds() image.Rectangle {
	width := int(img.ximg.width)
	height := int(img.ximg.height)
	return image.Rect(0, 0, width, height)
}

// At returns the color of the pixel at (x, y).
// At(Bounds().Min.X, Bounds().Min.Y) returns the upper-left pixel of the grid.
// At(Bounds().Max.X-1, Bounds().Max.Y-1) returns the lower-right one.
func (img Image) At(x, y int) color.Color {
	if x < 0 || y < 0 || x >= int(img.ximg.width) || y >= int(img.ximg.height) {
		return color.Gray{0x00}
	}
	red_mask := img.ximg.red_mask
	green_mask := img.ximg.green_mask
	blue_mask := img.ximg.blue_mask
	pixel := C.XGetPixel_go(img.ximg, C.int(x), C.int(y))
	blue := uint8(pixel & blue_mask)
	green := uint8((pixel & green_mask) >> 8)
	red := uint8((pixel & red_mask) >> 16)
	return color.RGBA{red, green, blue, 0xFF}
}

func (img Image) Free() {
	C.XFree(unsafe.Pointer(img.ximg))
}
