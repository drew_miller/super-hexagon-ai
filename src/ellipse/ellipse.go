package ellipse

import (
	"image"
	"image/color"
	"image/draw"
	"math"
	"vector"
)

// Good reference on ellipses
// http://www.cs.cornell.edu/cv/OtherPdf/Ellipse.pdf
type Circle struct {
	Center vector.Point
	Radius float64
}

type Ellipse struct {
	FocusA, FocusB vector.Point
	StringLength   float64
}

type EllipseParametric struct {
	X_scale, Y_scale float64
	Center           vector.Point
	Rotation         float64
}

func (circle *Circle) Draw(img draw.Image, clr color.Color) {
	for x := circle.Center.X - circle.Radius; x <= circle.Center.X+circle.Radius; x++ {
		for y := circle.Center.Y - circle.Radius; y <= circle.Center.Y+circle.Radius; y++ {
			point := vector.Pt(float64(x), float64(y))
			dist := vector.Distance(circle.Center, point)
			if math.Abs(circle.Radius-dist) <= 1 {
				img.Set(int(x), int(y), clr)
			}
		}
	}
}

func (circle *Circle) Fill(img draw.Image, clr color.Color) {
	rSquared := math.Pow(circle.Radius, 2)
	for y := circle.Radius; y >= 0; y-- {
		for x := math.Sqrt(rSquared - math.Pow(y, 2)); x >= 0; x-- {
			img.Set(int(circle.Center.X-x), int(circle.Center.Y-y), clr)
			img.Set(int(circle.Center.X-x), int(circle.Center.Y+y), clr)
			img.Set(int(circle.Center.X+x), int(circle.Center.Y-y), clr)
			img.Set(int(circle.Center.X+x), int(circle.Center.Y+y), clr)
		}
	}
}

func (ell *Ellipse) Fill(img draw.Image, clr color.Color) {
	leftX := math.Floor(math.Min(ell.FocusA.X, ell.FocusB.X) - ell.StringLength/2)
	rightX := math.Ceil(math.Max(ell.FocusA.X, ell.FocusB.X) + ell.StringLength/2)
	bottomY := math.Floor(math.Min(ell.FocusA.Y, ell.FocusB.Y) - ell.StringLength/2)
	topY := math.Ceil(math.Max(ell.FocusA.Y, ell.FocusB.Y) + ell.StringLength/2)
	for y := bottomY; y <= topY; y++ {
		for x := leftX; x <= rightX; x++ {
			if ell.Contains(x, y) {
				img.Set(int(x), int(y), clr)
			}
		}
	}
}

func (ell *Ellipse) Contains(x, y float64) bool {
	return EllipseStringLength(ell.FocusA, ell.FocusB, vector.Pt(x, y)) <= ell.StringLength
}

func (ell *Ellipse) DrawEllipse(img draw.Image) {
	bounds := img.Bounds()
	for x := bounds.Min.X; x < bounds.Max.X; x++ {
		for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
			point := vector.Pt(float64(x), float64(y))
			dist := EllipseStringLength(ell.FocusA, ell.FocusB, point)
			if math.Abs(ell.StringLength-dist) <= 1 {
				img.Set(x, y, color.Gray{0x8F})
			}
		}
	}
	img.Set(int(ell.FocusA.X), int(ell.FocusA.Y), color.Gray{0x8F})
	img.Set(int(ell.FocusB.X), int(ell.FocusB.Y), color.Gray{0x8F})
}

func (circ *Circle) RenderCircle(img draw.Image, clr color.Color) {
	for x := int(circ.Center.X - circ.Radius); x < int(circ.Center.X+circ.Radius); x++ {
		for y := int(circ.Center.Y - circ.Radius); y < int(circ.Center.Y+circ.Radius); y++ {
			point := vector.Pt(float64(x), float64(y))
			dist := vector.Distance(circ.Center, point)
			if math.Abs(circ.Radius-dist) <= 1 {
				img.Set(x, y, clr)
			}
		}
	}
}

func New(focusA, focusB vector.Point, strlen float64) *Ellipse {
	return &Ellipse{focusA, focusB, strlen}
}

func (ell Ellipse) Area() float64 {
	dist := vector.Distance(ell.FocusA, ell.FocusB)
	return math.Pi / 4 * ell.StringLength * math.Sqrt(math.Pow(ell.StringLength, 2)-math.Pow(dist, 2))
}

func (ell Ellipse) Empty() bool {
	emptyPoint := vector.Pt(0, 0)
	return (ell.FocusA == emptyPoint) && (ell.FocusB == emptyPoint) && (ell.StringLength == 0)
}

func (ell Ellipse) Parametric() *EllipseParametric {
	parametric := new(EllipseParametric)
	parametric.X_scale = ell.StringLength / 2
	parametric.Y_scale = math.Sqrt(
		math.Pow(ell.StringLength, 2)-
			(math.Pow(float64(ell.FocusA.X-ell.FocusB.X), 2)+
				math.Pow(float64(ell.FocusA.Y-ell.FocusB.Y), 2))) / 2
	parametric.Center.X = (ell.FocusA.X + ell.FocusB.X) / 2
	parametric.Center.Y = (ell.FocusA.Y + ell.FocusB.Y) / 2
	parametric.Rotation = math.Atan((ell.FocusB.Y - ell.FocusA.Y) / (ell.FocusB.X - ell.FocusA.X))
	return parametric
}

func EllipseStringLength(focusA, focusB, point vector.Point) float64 {
	return vector.Distance(focusA, point) + vector.Distance(point, focusB)
}

func LongestEllipseStringLength(focusA, focusB vector.Point, points []vector.Point) float64 {
	longestLength := float64(0)
	for _, point := range points {
		length := EllipseStringLength(focusA, focusB, point)
		if length > longestLength {
			longestLength = length
		}
	}
	return longestLength
}

func SmallestEllipseContainingPoints_migrate(
	points []vector.Point,
	p, q vector.Point,
	step float64) *Ellipse {
	bestEllipse := new(Ellipse)
	currEllipse := new(Ellipse)
	bestArea := math.MaxFloat64
	for p_xd := -step; p_xd < step*1.5; p_xd += step {
		p_xi := p.X + p_xd
		for p_yd := -step; p_yd <= step*1.5; p_yd += step {
			p_yi := p.Y + p_yd
			currEllipse.FocusA = vector.Pt(p_xi, p_yi)
			for q_xd := -step; q_xd < step*1.5; q_xd += step {
				q_xi := q.X + q_xd
				for q_yd := -step; q_yd < step*1.5; q_yd += step {
					q_yi := q.Y + q_yd
					currEllipse.FocusB = vector.Pt(q_xi, q_yi)
					currEllipse.StringLength = LongestEllipseStringLength(currEllipse.FocusA, currEllipse.FocusB, points)
					currArea := currEllipse.Area()
					if currArea <= bestArea {
						*bestEllipse = *currEllipse
						bestArea = currArea
					}
				}
			}
		}
	}
	if math.Abs(bestEllipse.FocusA.X-p.X) <= 1 &&
		math.Abs(bestEllipse.FocusA.Y-p.Y) <= 1 &&
		math.Abs(bestEllipse.FocusB.X-q.X) <= 1 &&
		math.Abs(bestEllipse.FocusB.Y-q.Y) <= 1 {
		if step < 1 {
			return bestEllipse
		} else {
			return SmallestEllipseContainingPoints_migrate(points, bestEllipse.FocusA, bestEllipse.FocusB, step/2)
		}
	} else {
		return SmallestEllipseContainingPoints_migrate(points, bestEllipse.FocusA, bestEllipse.FocusB, step)
	}
}

func SmallestEllipseContainingPoints(points []vector.Point) *Ellipse {
	a, b := vector.MostDistantPointPair(points)
	mid := vector.Midpoint(a, b)
	step := math.Max(1.0, vector.Distance(a, b)/8)
	ell := SmallestEllipseContainingPoints_migrate(points, mid, mid, step)
	return ell
}

func Corners(img draw.Image, center image.Point) []vector.Point {
	radiusMax := math.Sqrt(math.Pow(float64(center.X), 2)+math.Pow(float64(center.Y), 2)) / 2
	var radii [20]float64
	centerPt := img.At(center.X, center.Y)
	for th_i := 0; th_i < len(radii); th_i++ {
		blocked := false
		th := (float64(th_i*2) * math.Pi) / float64(len(radii))
		for radii[th_i] = 0; radii[th_i] < radiusMax; radii[th_i]++ {
			r := radii[th_i] + 1
			x := center.X + int(r*math.Cos(th))
			y := center.Y + int(r*math.Sin(th))
			for y_i := y - 1; y_i <= y+1; y_i++ {
				for x_i := x - 1; x_i <= x+1; x_i++ {
					pt := image.Pt(x, y)
					if img.At(pt.X, pt.Y) != centerPt ||
						img.At(pt.X-1, pt.Y) != centerPt ||
						img.At(pt.X, pt.Y-1) != centerPt ||
						img.At(pt.X+1, pt.Y) != centerPt ||
						img.At(pt.X, pt.Y+1) != centerPt {
						blocked = true
						break
					}
				}
			}
			if blocked {
				break
			}
		}
		if !blocked {
			radii[th_i] = 0
		}
	}
	corners := make([]vector.Point, 0, len(radii))
	for th_i := 0; th_i <= len(radii); th_i++ {
		if th_i%(cap(radii)/10) == 0 ||
			radii[th_i%len(radii)] > radii[(th_i-1+len(radii))%len(radii)] &&
				radii[th_i%len(radii)] >= radii[(th_i+1)%len(radii)] {
			th := ((float64(th_i%len(radii)) * 2) * math.Pi) / float64(len(radii))
			corners = corners[0 : len(corners)+1]
			corners[len(corners)-1] = vector.Pt(
				float64(center.X)+radii[th_i%len(radii)]*math.Cos(th),
				float64(center.Y)+radii[th_i%len(radii)]*math.Sin(th))
		}
	}
	return corners
}
