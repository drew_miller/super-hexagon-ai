package interpClosest

import (
	"code.google.com/p/graphics-go/graphics/interp"
	"image"
	"image/color"
)

type closest struct{}

var Closest interp.Interp = closest{}

func (closest) Interp(src image.Image, x, y float64) color.Color {
	return src.At(int(x), int(y))
}
