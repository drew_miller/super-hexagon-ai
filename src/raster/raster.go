package raster

import (
	"image"
	"image/color"
	"image/draw"
	"math"
)

func Brightness(clr color.Color) uint8 {
	return color.GrayModel.Convert(clr).(color.Gray).Y
}

func Center(img draw.Image) image.Point {
	bounds := img.Bounds()
	return image.Pt((bounds.Min.X+bounds.Max.X)/2,
		(bounds.Min.Y+bounds.Max.Y)/2)
}

func FloodFill4(img draw.Image, x, y int, newColor color.Color) (blobCenter image.Point, size int) {
	return FloodFill(img, x, y, false, newColor)
}

func FloodFill8(img draw.Image, x, y int, newColor color.Color) (blobCenter image.Point, size int) {
	return FloodFill(img, x, y, true, newColor)
}

func FloodFill(img draw.Image, x, y int, diagonal bool, newColor color.Color) (blobCenter image.Point, size int) {
	xSum := 0
	ySum := 0
	ptCount := 0
	oldColor := img.At(x, y)
	stackRect := make([]image.Point, 0, 4*(img.Bounds().Size().X+img.Bounds().Size().Y))
	stackRect = append(stackRect, image.Pt(x, y))
	stackDiag := make([]image.Point, 0, 4*(img.Bounds().Size().X+img.Bounds().Size().Y))
	for {
		var pt image.Point
		if len(stackRect) > 0 {
			pt = stackRect[len(stackRect)-1]
			stackRect = stackRect[:len(stackRect)-1]
		} else if len(stackDiag) > 0 {
			pt = stackDiag[len(stackDiag)-1]
			stackDiag = stackDiag[:len(stackDiag)-1]
		} else {
			break
		}
		x, y = pt.X, pt.Y
		for y >= img.Bounds().Min.Y && img.At(x, y) == oldColor {
			y--
		}
		y++
		spanLeft, spanRight := false, false
		for y < img.Bounds().Max.Y && img.At(x, y) == oldColor {
			img.Set(x, y, newColor)
			xSum += x
			ySum += y
			ptCount++
			if x > img.Bounds().Min.X {
				isOldColor := img.At(x-1, y) == oldColor
				if !spanLeft && isOldColor {
					stackRect = append(stackRect, image.Pt(x-1, y))
					spanLeft = true
				} else if spanLeft && !isOldColor {
					spanLeft = false
				}
				if !isOldColor {
					if diagonal {
						stackDiag = append(stackDiag, image.Pt(x-1, y-1))
						stackDiag = append(stackDiag, image.Pt(x-1, y+1))
					}
				}
			}
			if x < img.Bounds().Max.X-1 {
				isOldColor := img.At(x+1, y) == oldColor
				if !spanRight && isOldColor {
					stackRect = append(stackRect, image.Pt(x+1, y))
				} else if spanRight && !isOldColor {
					spanRight = false
				}
				if !isOldColor {
					if diagonal {
						stackDiag = append(stackDiag, image.Pt(x+1, y-1))
						stackDiag = append(stackDiag, image.Pt(x+1, y+1))
					}
				}
			}
			y++
		}
	}
	if ptCount <= 0 {
		return image.Pt(x, y), ptCount
	} else {
		return image.Pt(xSum/ptCount, ySum/ptCount), ptCount
	}
}

func Midpoint(a, b image.Point) image.Point {
	return image.Pt((a.X+b.X)/2, (a.Y+b.Y)/2)
}

func Distance(a, b image.Point) float64 {
	return math.Sqrt(
		math.Pow(float64(a.X)-float64(b.X), 2) +
			math.Pow(float64(a.Y)-float64(b.Y), 2))
}

func RectangleContains(r image.Rectangle, pt image.Point) bool {
	return (pt.X >= r.Min.X) && (pt.X < r.Max.X) && (pt.Y >= r.Min.Y) && (pt.Y < r.Max.Y)
}

func RectangleCenter(r image.Rectangle) image.Point {
	return Midpoint(r.Min, r.Max)
}
