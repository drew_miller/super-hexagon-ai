package vector

import (
	"image"
	"math"
)

func (pt *Point) ToImagePt() image.Point {
	return image.Pt(int(pt.X), int(pt.Y))
}

type Point struct {
	X, Y float64
}

func Pt(x, y float64) Point {
	pt := new(Point)
	pt.X = x
	pt.Y = y
	return *pt
}

func ImagePtToVectorPt(pt image.Point) Point {
	return Pt(float64(pt.X), float64(pt.Y))
}

func Midpoint(p, q Point) Point {
	return Pt(float64(p.X+q.X)/2, float64(p.Y+q.Y)/2)
}

func Distance(a, b Point) float64 {
	return math.Sqrt(math.Pow(float64(a.X-b.X), 2) + math.Pow(float64(a.Y-b.Y), 2))
}

func RectangleContains(r image.Rectangle, pt Point) bool {
	return (pt.X > float64(r.Min.X)) && (pt.X < float64(r.Max.X)) &&
		(pt.Y > float64(r.Min.Y)) && (pt.Y < float64(r.Max.Y))
}

func Angle(center, target Point) float64 {
	dX := target.X - center.X
	dY := target.Y - center.Y
	if dX == 0 {
		if dY > 0 {
			return math.Pi
		} else if dY < 0 {
			return -math.Pi
		} else {
			return 0
		}
	}
	angle := math.Atan(dY / dX)
	if dX < 0 {
		angle += math.Pi
	}
	return angle
}

func MostDistantPointPair(points []Point) (a, b Point) {
	greatestDistance := float64(0)
	for p_i, p := range points {
		for q_i, q := range points {
			if q_i <= p_i {
				continue
			} else {
				distance := Distance(p, q)
				if distance >= greatestDistance {
					greatestDistance = distance
					a, b = p, q
				}
			}
		}
	}
	return a, b
}
